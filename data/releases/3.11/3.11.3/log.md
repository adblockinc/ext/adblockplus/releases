- 2021-09-15
  - Received notification of upcoming snippet changes.
- 2021-10-22
  - Published internal release announcement.
  - Created release notes draft.
- 2021-10-28
  - Received notification that testing has finished.
- 2021-11-01
  - Received confirmation that testing has finished.
- 2021-11-02
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
  - Created MR for downloads repo.
  - Published to CWS.
  - Failed to publish to AMO:
    File is too large to parse.
    Error: This file is not binary and is too large to parse. Files larger than 4MB will not be parsed. Consider moving large lists of data out of JavaScript files and into JSON files, or splitting very large files into smaller ones.
    background.js
  - Deferred publishing for CWS.
- 2021-11-23
  - Published release notes.
  - Noticed that release is live on AMO.
  - Noticed taht release is live on CWS.
  - Published to downloads repo.
- 2021-11-24
  - Noticed that release is live on Microsoft store.
- 2021-12-02
  - Noticed that release is live on Opera store.
