- 2020-09-24
  - Asked for non-binding plans for changes to include in release.
- 2021-11-23
  - Published internal release announcement.
  - Created release notes draft.
- 2022-01-11
  - Received notification that testing has finished.
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
  - Created MR for downloads repo.
  - Published to CWS.
  - Failed to publish to AMO. Requires accepting to updated distribution agreement and policies.
  - Published to Microsoft store.
  - Published to Opera store.
- 2022-01-12
  - Noticed that release is live on CWS.
  - Noticed that release is live on Opera store.
- 2022-01-13
  - Noticed that release is live on Microsoft store.
- 2022-01-17
  - Published to downloads repo.
- 2022-05-03
  - Received approval for accepting AMO's updated policies.
  - Published to AMO with note for reviewers:
    Run the following commands using Node 12.17.0:
      cd adblockpluschrome
      npm install
      npx gulp build -t firefox -c release
- 2022-05-04
  - Noticed that release is live on AMO.
