- 2024-03-13
  - Set preliminary feature freeze date to 2024-03-27.
  - Set preliminary release date to 2024-04-02.
- 2024-03-27
  - Set preliminary feature freeze date to 2024-04-10.
  - Set preliminary release date to 2024-04-16.
- 2024-04-10
  - Set preliminary feature freeze date to 2024-04-24.
  - Set preliminary release date to 2024-04-30.
- 2024-04-24
  - Defined scope.
  - Created release notes draft.
. 2024-04-29
  - Received notification that testing has finished.
  - Updated rules data.
- 2024-04-30
  - Published to downloads repo. Encountered warning message when pushing to
    git repository, but confirmed that push was successful:
      You have reached the free storage limit of 10 GiB on one or more
      projects. To unlock your projects over the free 10 GiB project
      limit, you must purchase additional storage. You can't push to your
      repository, create pipelines, create issues, or add comments. To
      reduce storage capacity, you can delete unused repositories,
      artifacts, wikis, issues, and pipelines.
  - Published to AMO.
  - Published to Microsoft store.
  - Published to Opera store.
  - Generated Manifest v3 development build 4.0.11233.
  - Published development build to CWS.
  - Noticed that release is live on Opera store.
  - Published to CWS.
- 2024-05-01
  - Noticed that release is live on Microsoft store.
- 2024-05-02
  - Noticed that release is live on CWS.
  - Noticed that development build is live on CWS.
- 2024-05-07
  - Noticed that release is live on AMO.
- 2024-05-16
  - Generated filters-only build 4.0.0.1.
  - Generated filters-only development build 4.0.11235.
  - Published build to CWS.
  - Published development build to CWS.
- 2024-05-17
  - Noticed that filters-only build is live on CWS.
  - Noticed that development build is live on CWS.
