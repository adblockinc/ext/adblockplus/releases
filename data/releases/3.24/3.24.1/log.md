- 2024-01-31
  - Set preliminary feature freeze date to 2024-02-14.
  - Set preliminary release date to 2024-02-20.
- 2024-02-14
  - Defined scope.
  - Created release notes draft.
- 2024-02-15
  - Received notification that testing has finished.
- 2024-02-20
  - Published to downloads repo.
  - Published to CWS.
  - Published to Microsoft store.
  - Published to Opera store.
- 2024-02-21
  - Noticed that release is live on Opera store.
  - Noticed that release is live on CWS.
  - Noticed that release is live on Microsoft store.
- 2024-02-22
  - Generated Manifest v3 development build 3.24.11180.
  - Tested Manifest v3 upgrade using https://github.com/GoogleChromeLabs/extension-update-testing-tool .
  - Found that sitekey allowlisting doesn't work.
  - CWS: Found permission changes:
    old: contextMenus, notifications, storage, tabs, unlimitedStorage, webNavigation, webRequest, webRequestBlocking, contentSettings, management, host permission
    new: contextMenus, declarativeNetRequestWithHostAccess, notifications, scripting, storage, tabs, unlimitedStorage, webNavigation, webRequest, contentSettings, management, host permission
  - CWS: Unable to publish due to:
    - A justification for declarativeNetRequestWithHostAccess is required. This can be entered on the Privacy practices tab.
    - A justification for scripting is required. This can be entered on the Privacy practices tab.
  - CWS: Added justifications for new permissions:
    - declarativeNetRequestWithHostAccess
      - Blocking network requests that are related to ads.
      - Blocking network requests based on users' custom rules and other user settings.
    - scripting
      Injecting (user) style-sheets into pages and providing contextual information, both required to block ads effectively
    - tabs
      Providing contextual information, required to block ads effectively
    - webRequest
      Recording requests for diagnostics in the devtools panel
  - Published development build to CWS.
  - Noticed that development build is live on CWS.
- 2024-02-27
  - Generated Manifest v3 development build 3.24.11181.
  - Published development build to CWS.
- 2024-02-28
  - Noticed that development build is live on CWS.
