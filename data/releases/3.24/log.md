- 2024-01-17
  - Set preliminary feature freeze date to 2024-01-31.
  - Set preliminary release date to 2024-02-06.
- 2024-01-31
  - Defined scope.
  - Published internal release announcement.
  - Created release notes draft.
- 2024-02-02
  - Received notification that testing has finished.
- 2024-02-05
  - Published to downloads repo.
  - Published to CWS.
  - Published to AMO.
  - Published to Microsoft store.
  - Published to Opera store.
  - Published development build to CWS.
  - Received rejection notification from CWS:
    Having excessive and / or irrelevant keywords in the item's description
  - Removed user reviews from extension description in CWS store page and published to CWS again.
  - Noticed that release is live on Opera store.
- 2024-02-06
  - Received rejection notification from CWS:
    Having excessive and / or irrelevant keywords in the item's description
  - Reached out to CWS support for clarification.
- 2024-02-07
  - Noticed that release is live on Microsoft store.
  - Received reply to appeal from CWS support:
    Upon a subsequent review, we found that your item is not compliant with our Developer Program policy.
    User testimonials are provided in the extension’s description.
  - Double-checked that there is no violation and published to CWS again.
- 2024-02-08
  - Noticed that release is live on CWS.
  - Received rejection notification from AMO:
    The data collection consent experience needs to contain a summary of the data collection.
  - Decided to accept AMO's rejection for 3.24 and to work on addressing their concerns in a subsequent release.
  - Updated public release notes to reflect decision about not publishing 3.24 for Firefox.
  - Received additional information from AMO:
    The control mechanism must be shown at first-run of the add-on and need to be in an extension page, not in a preferences page.
  - Replied to AMO, acknowledging the rejection.
