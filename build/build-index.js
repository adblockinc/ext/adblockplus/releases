"use strict";

const fs = require("fs");
const path = require("path");
const {promisify} = require("util");

const {crawl} = require("./crawler");
const releaseOrder = require("../data/order.json");

const fsReadFile = promisify(fs.readFile);
const fsWriteFile = promisify(fs.writeFile);

const dirpathInput = "./data/";
const dirpathOutput = "./public/data/";
const filenameIndex = "index.json";
const filenamePerformance = "performance.json";

function compareVersions(versionA, versionB)
{
  if (releaseOrder.includes(versionA) && releaseOrder.includes(versionB))
    return releaseOrder.indexOf(versionA) - releaseOrder.indexOf(versionB);

  const partsA = versionA.split(".");
  const partsB = versionB.split(".");

  const maxLength = Math.max(partsA.length, partsB.length);
  for (let i = 0; i < maxLength; i++)
  {
    const partA = parseInt(partsA[i] || "0", 10);
    const partB = parseInt(partsB[i] || "0", 10);

    if (partA === partB)
      continue;

    return partA - partB;
  }
}

function getDate(str)
{
  if (!str || !/^\d{4}-\d{2}-\d{2}$/.test(str))
    return null;

  return new Date(str);
}

function getDurations(milestones, approvals)
{
  const durations = {};

  const now = new Date();
  const created = getDate(milestones.created);
  const featurefreeze = getDate(milestones.featurefreeze);
  const codefreeze = getDate(milestones.codefreeze);
  const release = getDate(milestones.release);
  const closed = getDate(milestones.closed);

  if (created)
  {
    durations.development = (featurefreeze || now) - created;
  }
  if (featurefreeze)
  {
    durations.featurefreeze = (codefreeze || now) - featurefreeze;
  }
  if (codefreeze)
  {
    durations.codefreeze = (release || now) - codefreeze;
  }
  if (release)
  {
    durations.storereview = (closed || now) - release;
  }

  const scope = getDate(approvals.scope);
  const notes = getDate(approvals.notes);
  const qa = getDate(approvals.qa);
  const google = getDate(approvals.stores.google);
  const microsoft = getDate(approvals.stores.microsoft);
  const mozilla = getDate(approvals.stores.mozilla);
  const opera = getDate(approvals.stores.opera);

  if (scope && scope !== true)
  {
    durations.scope = scope - created;
  }
  if (notes && notes !== true)
  {
    durations.notes = notes - featurefreeze;
  }
  if (qa && qa !== true)
  {
    durations.qa = qa - featurefreeze;
  }

  if (release)
  {
    durations.google = (google || closed || now) - release;
    durations.microsoft = (microsoft || closed || now) - release;
    durations.mozilla = (mozilla || closed || now) - release;
    durations.opera = (opera || closed || now) - release;
  }

  for (let name in durations)
  {
    durations[name] = durations[name] / 1000 / 60 / 60 / 24;
  }

  return durations;
}

function getPerformance(releaseInfos)
{
  return releaseInfos.map((releaseInfo) =>
  {
    return {
      version: releaseInfo.version,
      durations: getDurations(releaseInfo.milestones, releaseInfo.approvals)
    }
  });
}

async function run()
{
  const releaseInfos = await crawl(
    [],
    "3",
    path.join(dirpathInput, "releases")
  );
  const releaseIndex = releaseInfos
    .map((releaseInfo) =>
    {
      return {
        version: releaseInfo.version,
        milestones: releaseInfo.milestones
      };
    })
    .sort((a, b) => compareVersions(a.version, b.version));

  for (const releaseInfo of releaseInfos)
  {
    const releaseFilepath = path.join(
      dirpathOutput,
      "releases",
      `${releaseInfo.version}.json`
    );
    await fsWriteFile(releaseFilepath, JSON.stringify(releaseInfo), "utf8");
    console.log(`Created '${releaseFilepath}'`);

    if (releaseInfo.milestones.featurefreeze &&
        /^\d{4}-\d{2}-\d{2}$/.test(releaseInfo.milestones.featurefreeze) &&
        releaseOrder.includes(releaseInfo.version))
      throw new Error("Order only applies to releases in development");
  }

  const blockersContent = await fsReadFile(
    path.join(dirpathInput, "blockers.json")
  );
  const blockers = JSON.parse(blockersContent);

  const indexFilepath = path.join(dirpathOutput, filenameIndex);
  const index = {
    blockers,
    releases: releaseIndex
  };
  await fsWriteFile(indexFilepath, JSON.stringify(index), "utf8");
  console.log(`Created index file '${indexFilepath}'`);

  const performanceFilepath = path.join(dirpathOutput, filenamePerformance);
  const performance = {
    releases: getPerformance(releaseInfos)
      .sort((a, b) => compareVersions(a.version, b.version))
  };
  await fsWriteFile(performanceFilepath, JSON.stringify(performance), "utf8");
  console.log(`Create performance file '${performanceFilepath}'`);
}

run().catch(console.error);
