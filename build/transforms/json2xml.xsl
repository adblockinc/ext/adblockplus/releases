<?xml version="1.0"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="xs" version="3.0">
  <xsl:output indent="yes" omit-xml-declaration="yes"/>

  <xsl:template match="data">
    <xsl:apply-templates mode="copy" select="json-to-xml(.)"/>
  </xsl:template>

  <!-- Copy elements to remove namespace -->
  <xsl:template match="*" mode="copy">
    <xsl:element name="{name()}">
      <xsl:apply-templates select="@*|node()" mode="copy" />
    </xsl:element>
  </xsl:template>

  <xsl:template match="@*|text()|comment()" mode="copy">
    <xsl:copy/>
  </xsl:template>
</xsl:stylesheet>
