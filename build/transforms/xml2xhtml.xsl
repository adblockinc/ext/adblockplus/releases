<?xml version="1.0"?>
<xsl:stylesheet
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:io="http://eyeo.com/ext-release-docs"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="io xs" version="3.0">
  <xsl:output method="xhtml" indent="yes" omit-xml-declaration="yes"/>

  <io:platforms>
    <io:platform id="chrome" href="https://chrome.google.com/webstore/detail/cfhdojbkjhnklbpkdaibdccddilifddb">Chrome</io:platform>
    <io:platform id="edge" href="https://microsoftedge.microsoft.com/addons/detail/gmgoamodcdcjnbaobigkjelfplakmdhh">Microsoft Edge</io:platform>
    <io:platform id="firefox" href="https://addons.mozilla.org/firefox/addon/adblock-plus/">Firefox</io:platform>
    <io:platform id="opera" href="https://addons.opera.com/extensions/details/opera-adblock/">Opera</io:platform>
  </io:platforms>

  <xsl:key name="links" match="/map/map[@key='release']/map[@key='notes']/map[@key='links']/string" use="@key"/>
  <xsl:key name="platforms" match="/*/io:platforms/io:platform" use="@id"/>
  <xsl:variable name="version" select="/map/map[@key='release']/string[@key='version']"/>

  <xsl:template match="/map">
    <html>
      <head>
        <title>
          <xsl:call-template name="headline"/>
        </title>
      </head>
      <body>
        <h1>
          <xsl:call-template name="headline"/>
        </h1>

        <!-- Platform links -->
        <xsl:if test="string[@key='visibility'] = 'external'">
          <xsl:for-each select="map[@key='release']/array[@key='platforms']/string">
            <xsl:sort select="key('platforms', ., document(''))" data-type="string"/>
            <div>
              <xsl:call-template name="platform-link">
                <xsl:with-param name="platform">
                  <xsl:value-of select="."/>
                </xsl:with-param>
              </xsl:call-template>
            </div>
          </xsl:for-each>
        </xsl:if>

        <!-- Store review note -->
        <xsl:if test="string[@key='visibility'] = 'external'">
          <p>
            <strong>Note:</strong>
            <xsl:text> Due to third-party store review policies, it may take a while until the update is made available to everyone.</xsl:text>
          </p>
        </xsl:if>

        <!-- Feature freeze announcement -->
        <xsl:if test="string[@key='visibility'] = 'internal'">
          <p>
            <xsl:text>We just entered feature freeze for Adblock Plus </xsl:text>
            <xsl:value-of select="$version"/>
            <xsl:text>. As usual, the release date is still to be discussed, based on how testing goes.</xsl:text>
          </p>
        </xsl:if>

        <!-- Summary -->
        <xsl:if test="map[@key='release']/map[@key='notes']/string[@key='summary']">
          <p>
            <xsl:call-template name="inline-markup">
              <xsl:with-param name="text" select="map[@key='release']/map[@key='notes']/string[@key='summary']"/>
            </xsl:call-template>
          </p>
        </xsl:if>
        <xsl:if test="map[@key='release']/map[@key='notes']/array[@key='summary']">
          <xsl:for-each select="map[@key='release']/map[@key='notes']/array[@key='summary']/string">
            <p>
              <xsl:call-template name="inline-markup">
                <xsl:with-param name="text" select="."/>
              </xsl:call-template>
            </p>
          </xsl:for-each>
        </xsl:if>

        <!-- Changes -->
        <xsl:for-each select="map[@key='release']/map[@key='notes']/array[@key='changes']/map">
          <xsl:call-template name="notes-group">
            <xsl:with-param name="level">0</xsl:with-param>
          </xsl:call-template>
        </xsl:for-each>

        <!-- Internal links -->
        <xsl:if test="string[@key='visibility'] = 'internal'">
          <p>
            <xsl:text>Interested people may want to try out the development builds which include all changes to be released.</xsl:text>
          </p>
        </xsl:if>
      </body>
    </html>
  </xsl:template>

  <!-- Headline -->
  <xsl:template name="headline">
    <xsl:text>Adblock Plus </xsl:text>
    <xsl:value-of select="$version"/>
    <xsl:text> for </xsl:text>
    <xsl:for-each select="map[@key='release']/array[@key='platforms']/string">
      <xsl:sort select="key('platforms', ., document(''))" data-type="string"/>
      <xsl:choose>
        <xsl:when test="position() = count(../string)"> and </xsl:when>
        <xsl:when test="position() > 1">, </xsl:when>
      </xsl:choose>
      <xsl:value-of select="key('platforms', ., document(''))"/>
    </xsl:for-each>
  </xsl:template>

  <!-- Platform links -->
  <xsl:template name="platform-link">
    <xsl:param name="platform" as="xs:string"/>

    <a>
      <xsl:attribute name="href">
        <xsl:value-of select="key('platforms', $platform, document(''))/[@href]"/>
      </xsl:attribute>

      <xsl:text>Install Adblock Plus </xsl:text>
      <xsl:value-of select="$version"/>
      <xsl:text> for </xsl:text>
      <xsl:value-of select="key('platforms', $platform, document(''))"/>
    </a>
  </xsl:template>

  <!-- Text with inline markup -->
  <xsl:template name="inline-markup">
    <xsl:param name="text" as="xs:string"/>
    <xsl:variable name="doc" select="/"/>

    <!-- Parse link markup -->
    <xsl:analyze-string select="$text" regex="\[([^\]]+)\](?:\[([^\]]+)\])?">
      <xsl:matching-substring>
        <a>
          <xsl:attribute name="href">
            <!-- Resolve URL -->
            <xsl:choose>
              <xsl:when test="regex-group(2)">
                <!-- Custom link -->
                <xsl:value-of select="key('links', regex-group(2), $doc)"/>
              </xsl:when>
              <xsl:otherwise>
                <!-- Issue link -->
                <xsl:analyze-string select="regex-group(1)" regex="^([^#]+)#(\d+)$">
                  <xsl:matching-substring>
                    <xsl:choose>
                      <xsl:when test="regex-group(1) = 'abp'">https://gitlab.com/adblockinc/ext/adblockplus/adblockplus/-/issues/</xsl:when>
                      <xsl:when test="regex-group(1) = 'core'">https://gitlab.com/eyeo/adblockplus/abc/adblockpluscore/-/issues/</xsl:when>
                      <xsl:when test="regex-group(1) = 'ewe'">https://gitlab.com/eyeo/adblockplus/abc/webext-ad-filtering-solution/-/issues/</xsl:when>
                      <xsl:when test="regex-group(1) = 'snippets'">https://gitlab.com/eyeo/anti-cv/snippets/-/issues/</xsl:when>
                      <xsl:when test="regex-group(1) = 'ui'">https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/issues/</xsl:when>
                      <xsl:when test="regex-group(1) = 'webext'">https://gitlab.com/eyeo/adblockplus/adblockpluschrome/-/issues/</xsl:when>
                      <xsl:otherwise>
                        <xsl:text>#</xsl:text>
                        <xsl:value-of select="regex-group(1)"/>
                      </xsl:otherwise>
                    </xsl:choose>
                    <xsl:value-of select="regex-group(2)"/>
                  </xsl:matching-substring>
                </xsl:analyze-string>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <!-- Parse code markup inside link -->
          <xsl:analyze-string select="regex-group(1)" regex="`([^`]+)`">
            <xsl:matching-substring>
              <code>
                <xsl:value-of select="regex-group(1)"/>
              </code>
            </xsl:matching-substring>
            <xsl:non-matching-substring>
              <xsl:value-of select="."/>
            </xsl:non-matching-substring>
          </xsl:analyze-string>
        </a>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <!-- Parse code markup outside link -->
        <xsl:analyze-string select="." regex="`([^`]+)`">
          <xsl:matching-substring>
            <code>
              <xsl:value-of select="regex-group(1)"/>
            </code>
          </xsl:matching-substring>
          <xsl:non-matching-substring>
            <xsl:value-of select="."/>
          </xsl:non-matching-substring>
        </xsl:analyze-string>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>

  <!-- Notes group -->
  <xsl:template name="notes-group">
    <xsl:param name="level" as="xs:integer"/>

    <!-- Group title -->
    <xsl:choose>
      <xsl:when test="$level = 0">
        <h4>
          <xsl:value-of select="string[@key='title']"/>
        </h4>
        <xsl:if test="string[@key='subtitle']">
          <p>
            <xsl:call-template name="inline-markup">
              <xsl:with-param name="text" select="string[@key='subtitle']"/>
            </xsl:call-template>
          </p>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="inline-markup">
          <xsl:with-param name="text" select="string[@key='title']"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>

    <!-- Group items -->
    <ul>
      <xsl:for-each select="array[@key='items']/*">
        <li>
          <xsl:choose>
            <xsl:when test="self::string">
              <!-- Render item -->
              <xsl:call-template name="inline-markup">
                <xsl:with-param name="text" select="."/>
              </xsl:call-template>
            </xsl:when>
            <xsl:when test="self::array">
              <!-- Render multiline item -->
              <xsl:for-each select="string">
                <div>
                  <xsl:choose>
                    <xsl:when test="position() = 1">
                      <xsl:call-template name="inline-markup">
                        <xsl:with-param name="text" select="."/>
                      </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                      <em>
                        <xsl:call-template name="inline-markup">
                          <xsl:with-param name="text" select="."/>
                        </xsl:call-template>
                      </em>
                    </xsl:otherwise>
                  </xsl:choose>
                </div>
              </xsl:for-each>
            </xsl:when>
            <xsl:when test="self::map">
              <!-- Render group -->
              <xsl:call-template name="notes-group">
                <xsl:with-param name="level">1</xsl:with-param>
              </xsl:call-template>
            </xsl:when>
          </xsl:choose>
        </li>
      </xsl:for-each>
    </ul>
  </xsl:template>
</xsl:stylesheet>
