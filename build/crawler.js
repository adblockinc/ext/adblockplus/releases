"use strict";

const fs = require("fs");
const path = require("path");
const {promisify} = require("util");

const fsReadDir = promisify(fs.readdir);
const fsReadFile = promisify(fs.readFile);

const filenameLog = "log.md";
const filenameManifest = "manifest.json";
const filenameNotes = "notes.json";

async function getReleaseInfo(dirpath)
{
  const manifestContent = await fsReadFile(path.join(dirpath, filenameManifest));
  const manifest = JSON.parse(manifestContent);
  const notesContent = await fsReadFile(path.join(dirpath, filenameNotes));
  const notes = JSON.parse(notesContent);
  const log = await fsReadFile(path.join(dirpath, filenameLog));

  const output = manifest;
  output.logText = log.toString();
  output.notes = notes;

  return manifest;
}

async function crawl(releaseInfos, version, dirpath)
{
  const filenames = await fsReadDir(dirpath);

  for (const filename of filenames)
  {
    const filepath = path.join(dirpath, filename);
    switch (filename)
    {
      case filenameManifest:
        const releaseInfo = await getReleaseInfo(dirpath);
        releaseInfos.push(releaseInfo);
        break;
      case filenameLog:
      case filenameNotes:
        break;
      default:
        await crawl(releaseInfos, filename, filepath);
    }
  }

  return releaseInfos;
}
exports.crawl = crawl;
