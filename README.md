Adblock Plus Extension Releases
===============================

- [Requirements](#requirements)
- [Status dashboard](#status-dashboard)
- [Release information](#release-information)
- [Announcements](#announcements)

## Requirements

Install Node.js and run `npm install` to install all dependencies.

## Status dashboard

In order to run the status dashboard locally, you need Node.js installed and
run the following commands to prepare the necessary files and run a local
web server:

```bash
npm run pages
npm start
```

## Release information

Run `npm run $ create.release <version>` to create new release information.

Version examples:
- `1.0`
- `1.2`
- `1.2/1.2.3`
- `snippets-1.2`

## Announcements

Run `npm run $ create.announcement <version> <format> <visibility>` to render
the release announcement.

Output formats:
- `json`: Data as JSON
- `md`: Page as Markdown
- `txp`: Page as Textpattern code
- `xhtml`: Page as XHTML
- `xml`: Data as XML

Visibility:
- `external`: Public release notes
- `internal`: Internal release announcement

Rendering pipeline:
```mermaid
graph TD;
  json(JSON: Data) --> xml;
  xml(XML: Data) --> xhtml;
  xhtml(XHTML: Page structure / Code) --> md(Markdown: Code)
  xhtml --> txp(Textpattern: Code)
```
